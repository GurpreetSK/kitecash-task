# KiteCash - Task

 Use the GitHub API to get a list of users and display it as a list.
 When clicked on a user, the app should display the list of user's repositories.

#### Libraries used / Features implemented:

 1. RxJava
 3. Dagger 2
 4. MVP Architecture
 5. Unit Tests using JUnit4 and Mockito
 6. Pagination using InfiniteScrollListener
 7. Instrumentation Tests using Espresso
 8. Material Design
 9. Glide, Retrofit, OkHttp, ConstraintLayout, EventBus, Realm

#### APK

Click [here](https://bitbucket.org/GurpreetSK95/kitecash-task/raw/1b62f8e77366aab4a819dc695903a1f2525278ac/kitecash-prod-release.apk) to download


#### TODOs

- Implement Apk splits for small size ([large size due to Realm](https://academy.realm.io/posts/reducing-apk-size-native-libraries/))
