package me.gurpreetsk.kitecashtask.data.remote;

import java.util.List;

import io.reactivex.Observable;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

  @GET("/users")
  Observable<List<User>> getGithubUsers(@Query("since") long since);

  @GET("/users/{username}/repos")
  Observable<List<UserRepo>> getUserRepos(@Path("username") String username,
                                                    @Query("page") int page);

}
