package me.gurpreetsk.kitecashtask.data.remote;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import me.gurpreetsk.kitecashtask.data.local.DbHelper;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;

@Singleton
public class ApiHelper {

  @Inject
  public ApiHelper(ApiInterface client, DbHelper dbHelper) {
  }

  public Observable<List<User>> getUsersListFromServer(final int page) {
    return Observable.just(githubUsersList());
  }

  private List<User> githubUsersList() {
    User user = new User();
    user.setLogin("gurpreetsk95");
    User user1 = new User();
    user1.setLogin("harpreetsk");
    List<User> list = new ArrayList<>();
    list.add(user);
    list.add(user1);
    return list;
  }

  public Observable<List<UserRepo>> getUsersRepoFromServer(String userName, int page) {
    return Observable.just(githubUsersRepoList());
  }

  private List<UserRepo> githubUsersRepoList() {
    UserRepo repo = new UserRepo();
    repo.setName("my repo");
    UserRepo repo1 = new UserRepo();
    repo1.setName("my repo 1");
    List<UserRepo> list = new ArrayList<>();
    list.add(repo);
    list.add(repo1);
    return list;
  }


}
