package me.gurpreetsk.kitecashtask.data.remote;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;
import me.gurpreetsk.kitecashtask.data.events.DataFetchedEvent;
import me.gurpreetsk.kitecashtask.data.events.ErrorEvent;
import me.gurpreetsk.kitecashtask.data.events.RepoLoadEvent;
import me.gurpreetsk.kitecashtask.data.local.DbHelper;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;
import retrofit2.Response;

@Singleton
public class ApiHelper {

  private ApiInterface client;
  private DbHelper dbHelper;

  private static final String TAG = ApiHelper.class.getSimpleName();

  @Inject
  public ApiHelper(ApiInterface client, DbHelper dbHelper) {
    this.client = client;
    this.dbHelper = dbHelper;
  }

  public void getUsersListFromServer(final int page) {
    client.getGithubUsers(page * 30)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .retry(3)
        .subscribe(new Observer<Response<List<User>>>() {

          Disposable disposable;

          @Override
          public void onSubscribe(@NonNull Disposable disposable) {
            this.disposable = disposable;
          }

          @Override
          public void onNext(@NonNull Response<List<User>> listResponse) {
            if (listResponse.isSuccessful() && listResponse.code() == 200) {
              try {
                dbHelper.insertUserData(listResponse.body());
                EventBus.getDefault().post(new DataFetchedEvent("user"));
              } catch (RealmPrimaryKeyConstraintException rpke) {
                Log.e(TAG, "onNext: " + rpke.getLocalizedMessage());
              } catch (Exception e) {
                e.printStackTrace();
                EventBus.getDefault().post(new ErrorEvent(e.getLocalizedMessage()));
              }
            } else {
              Log.e(TAG, "onNext: " + listResponse.code());
              //TODO: show error to user
              EventBus.getDefault().post(new ErrorEvent("Couldn't fetch data, Please retry"));
              Log.e(TAG, "onNext: " + listResponse.errorBody());
            }
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG, "onError: ", e);
            //TODO: show error to user
            EventBus.getDefault().post(new ErrorEvent(e.getLocalizedMessage()));
          }

          @Override
          public void onComplete() {
            if (!disposable.isDisposed())
              disposable.dispose();
          }
        });
  }

  public void getUsersRepoFromServer(String userName, int page) {
    client.getUserRepos(userName, page)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .retry(3)
        .subscribe(new Observer<Response<List<UserRepo>>>() {
          Disposable disposable;

          @Override
          public void onSubscribe(@NonNull Disposable disposable) {
            this.disposable = disposable;
          }

          @Override
          public void onNext(@NonNull Response<List<UserRepo>> listResponse) {
            if (listResponse.isSuccessful() && listResponse.code() == 200) {
              EventBus.getDefault().post(new RepoLoadEvent(listResponse.body()));
            } else {
              Log.e(TAG, "onNext: " + listResponse.code());
              Log.e(TAG, "onNext: " + listResponse.errorBody());
              EventBus.getDefault().post(new ErrorEvent("Couldn't fetch data, Please retry"));
            }
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG, "onError: ", e);
            EventBus.getDefault().post(new ErrorEvent(e.getLocalizedMessage()));
          }

          @Override
          public void onComplete() {
            if (!disposable.isDisposed())
              disposable.dispose();
          }
        });
  }


}
