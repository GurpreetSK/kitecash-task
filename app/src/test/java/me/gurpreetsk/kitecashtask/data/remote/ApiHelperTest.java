package me.gurpreetsk.kitecashtask.data.remote;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Gurpreet on 15/09/17.
 */
public class ApiHelperTest {

  @Mock
  ApiInterface client;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getUsersFromServerTest() throws Exception {

    when(client.getGithubUsers(anyLong())).thenReturn(Observable.just(githubUserList()));

    //When
    TestObserver<List<User>> subscriber = new TestObserver<>();
    client.getGithubUsers(anyInt()).subscribe(subscriber);

    //Then
    subscriber.awaitTerminalEvent();
    subscriber.assertNoErrors();

    List<List<Object>> onNextEvents = subscriber.getEvents();
    List<Object> users = onNextEvents.get(0);
    Assert.assertEquals("gurpreetsk95", ((List<User>) users.get(0)).get(0).getLogin());
    Assert.assertEquals("harpreetsk", ((List<User>) users.get(0)).get(1).getLogin());
    verify(client).getGithubUsers(anyLong());

  }

  @Test
  public void emptyUserListTest() throws Exception {
    when(client.getGithubUsers(anyLong())).thenReturn(Observable.<List<User>>empty());

    //When
    TestObserver<List<User>> subscriber = new TestObserver<>();
    client.getGithubUsers(anyInt()).subscribe(subscriber);

    //Then
    subscriber.awaitTerminalEvent();
    subscriber.assertNoErrors();

    List<List<Object>> onNextEvents = subscriber.getEvents();
    List<Object> users = onNextEvents.get(0);
    Assert.assertTrue(users.isEmpty());
    verify(client).getGithubUsers(anyLong());
  }

  private List<User> githubUserList() {
    User user = new User();
    user.setLogin("gurpreetsk95");
    User user2 = new User();
    user2.setLogin("harpreetsk");
    List<User> githubUsers = new ArrayList<>();
    githubUsers.add(user);
    githubUsers.add(user2);
    return githubUsers;
  }

  @Test
  public void emptyUsersRepoTest() throws Exception {
    when(client.getUserRepos(anyString(), anyInt()))
        .thenReturn(Observable.<List<UserRepo>>empty());

    //When
    TestObserver<List<UserRepo>> subscriber = new TestObserver<>();
    client.getUserRepos(anyString(), anyInt()).subscribe(subscriber);

    //Then
    subscriber.awaitTerminalEvent();
    subscriber.assertNoErrors();

    List<List<Object>> onNextEvents = subscriber.getEvents();
    List<Object> repos = onNextEvents.get(0);
    Assert.assertTrue(repos.isEmpty());
    verify(client).getUserRepos(anyString(), anyInt());
  }

  @Test
  public void getUsersRepoFromServerTest() throws Exception {
    when(client.getUserRepos(anyString(), anyInt()))
        .thenReturn(Observable.just(githubUserRepoList()));

    //When
    TestObserver<List<UserRepo>> subscriber = new TestObserver<>();
    client.getUserRepos(anyString(), anyInt()).subscribe(subscriber);

    //Then
    subscriber.awaitTerminalEvent();
    subscriber.assertNoErrors();

    List<List<Object>> onNextEvents = subscriber.getEvents();
    List<Object> repos = onNextEvents.get(0);
    Assert.assertEquals("my repo", ((List<UserRepo>)repos.get(0)).get(0).getName());
    Assert.assertEquals("my repo 1", ((List<UserRepo>)repos.get(0)).get(1).getName());
    verify(client).getUserRepos(anyString(), anyInt());
  }

  private List<UserRepo> githubUserRepoList() {
    UserRepo repo = new UserRepo();
    repo.setName("my repo");
    UserRepo repo1 = new UserRepo();
    repo1.setName("my repo 1");
    List<UserRepo> userRepos = new ArrayList<>();
    userRepos.add(repo);
    userRepos.add(repo1);
    return userRepos;
  }

  @After
  public void tearDown() throws Exception {
  }

}