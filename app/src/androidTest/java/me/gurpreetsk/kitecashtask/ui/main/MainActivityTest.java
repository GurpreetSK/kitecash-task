package me.gurpreetsk.kitecashtask.ui.main;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import me.gurpreetsk.kitecashtask.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

  @Rule
  public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

  @Test
  public void mainActivityTest() {
    ViewInteraction imageView = onView(
        allOf(withId(R.id.imageview_user_avatar), withContentDescription("User's Github Avatar"),
            childAtPosition(
                allOf(withId(R.id.layout_user),
                    childAtPosition(
                        withId(R.id.recycler_view_users),
                        0)),
                0),
            isDisplayed()));
    imageView.check(matches(isDisplayed()));

    ViewInteraction textView = onView(
        allOf(withId(R.id.textview_user_login), withText("defunkt"),
            childAtPosition(
                allOf(withId(R.id.layout_user),
                    childAtPosition(
                        withId(R.id.recycler_view_users),
                        1)),
                1),
            isDisplayed()));
    textView.check(matches(isDisplayed()));

    ViewInteraction textView2 = onView(
        allOf(withId(R.id.textview_user_login), withText("mojombo"),
            childAtPosition(
                allOf(withId(R.id.layout_user),
                    childAtPosition(
                        withId(R.id.recycler_view_users),
                        0)),
                1),
            isDisplayed()));
    textView2.check(matches(withText("mojombo")));

    ViewInteraction imageButton = onView(
        allOf(withId(R.id.fab_goto_top),
            childAtPosition(
                allOf(withId(R.id.main_content),
                    childAtPosition(
                        withId(android.R.id.content),
                        0)),
                1),
            isDisplayed()));
    imageButton.check(matches(isDisplayed()));

    ViewInteraction textView3 = onView(
        allOf(withText("Github Users"),
            childAtPosition(
                allOf(withId(R.id.action_bar),
                    childAtPosition(
                        withId(R.id.action_bar_container),
                        0)),
                0),
            isDisplayed()));
    textView3.check(matches(withText("Github Users")));

    ViewInteraction textView4 = onView(
        allOf(withText("Github Users"),
            childAtPosition(
                allOf(withId(R.id.action_bar),
                    childAtPosition(
                        withId(R.id.action_bar_container),
                        0)),
                0),
            isDisplayed()));
    textView4.check(matches(withText("Github Users")));

  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
