package me.gurpreetsk.kitecashtask.ui.main;


import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import me.gurpreetsk.kitecashtask.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RepositoriesActivityTest {

  @Rule
  public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

  private IdlingResource idlingResource;

  @Before
  public void registerIdlingResource() {
    idlingResource = activityTestRule.getActivity().getIdlingResource();
    // To prove that the test fails, omit this call:
    Espresso.registerIdlingResources(idlingResource);
  }

  @Test
  public void repositoriesActivityTest() {
    ViewInteraction recyclerView = onView(
        allOf(withId(R.id.recycler_view_users),
            childAtPosition(
                allOf(withId(R.id.main_content),
                    childAtPosition(
                        withId(android.R.id.content),
                        0)),
                0),
            isDisplayed()));
    recyclerView.perform(actionOnItemAtPosition(2, click()));

    ViewInteraction viewGroup = onView(
        allOf(childAtPosition(
            allOf(withId(R.id.cardview_repo),
                childAtPosition(
                    withId(R.id.recycler_view_repos),
                    0)),
            0),
            isDisplayed()));
    viewGroup.check(matches(isDisplayed()));

    ViewInteraction textView2 = onView(
        allOf(withId(R.id.textview_repo_name), withText("auto_migrations"),
            childAtPosition(
                childAtPosition(
                    withId(R.id.cardview_repo),
                    0),
                0),
            isDisplayed()));
    textView2.check(matches(withText("auto_migrations")));

    ViewInteraction imageButton = onView(
        allOf(withId(R.id.fab_goto_repo_top),
            childAtPosition(
                allOf(withId(R.id.main_content),
                    childAtPosition(
                        withId(android.R.id.content),
                        0)),
                1),
            isDisplayed()));
    imageButton.check(matches(isDisplayed()));

  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }

  @After
  public void unregisterIdlingResource() {
    if (idlingResource != null) {
      Espresso.unregisterIdlingResources(idlingResource);
    }
  }

}
