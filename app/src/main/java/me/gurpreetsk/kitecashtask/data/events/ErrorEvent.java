package me.gurpreetsk.kitecashtask.data.events;

/**
 * Created by Gurpreet on 13/09/17.
 */

public class ErrorEvent {

  private String error;

  public ErrorEvent(String error) {
    this.error = error;
  }

  public String getError() {
    return error;
  }
}
