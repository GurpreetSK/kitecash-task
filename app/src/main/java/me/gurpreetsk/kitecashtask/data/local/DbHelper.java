package me.gurpreetsk.kitecashtask.data.local;

import android.content.Context;
import android.content.res.Resources;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.realm.Realm;
import io.realm.RealmResults;
import me.gurpreetsk.kitecashtask.dagger.ApplicationContext;
import me.gurpreetsk.kitecashtask.data.model.User;

/**
 * Created by Gurpreet on 12/09/17.
 */

@Singleton
public class DbHelper {

  private Context context;
  private Realm realm;

  @Inject
  public DbHelper(@ApplicationContext Context context, Realm realm) {
    this.context = context;
    this.realm = realm;
  }

  //TODO: check
  public RealmResults<User> getAllUsers()
      throws Resources.NotFoundException, NullPointerException {
    return realm.where(User.class).findAllSorted("id");
  }

  public int getUsersListSizeFromDb() {
    return getAllUsers().size();
  }

  public void insertUserData(final List<User> userList) throws Exception {
    realm.executeTransactionAsync(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.copyToRealmOrUpdate(userList);
      }
    });
  }

  public void deleteData(final long id) {
    realm.executeTransaction(new Realm.Transaction() {
      @Override
      public void execute(Realm realm) {
        realm.where(User.class).equalTo("id", id).findAll().deleteAllFromRealm();
      }
    });
  }

}
