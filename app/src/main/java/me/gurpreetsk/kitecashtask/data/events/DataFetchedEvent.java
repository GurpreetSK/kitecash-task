package me.gurpreetsk.kitecashtask.data.events;

/**
 * Created by Gurpreet on 13/09/17.
 */

public class DataFetchedEvent {

  private String type;

  public DataFetchedEvent(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

}
