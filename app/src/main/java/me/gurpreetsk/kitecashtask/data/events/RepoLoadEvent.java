package me.gurpreetsk.kitecashtask.data.events;

import java.util.List;

import me.gurpreetsk.kitecashtask.data.model.UserRepo;

/**
 * Created by Gurpreet on 12/09/17.
 */

public class RepoLoadEvent {

  private List<UserRepo> repoList;

  public RepoLoadEvent() {
  }

  public RepoLoadEvent(List<UserRepo> repoList) {
    this.repoList = repoList;
  }

  public List<UserRepo> getRepoList() {
    return repoList;
  }

  public void setRepoList(List<UserRepo> repoList) {
    this.repoList = repoList;
  }

}
