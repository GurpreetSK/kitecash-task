package me.gurpreetsk.kitecashtask.data;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.realm.RealmResults;
import me.gurpreetsk.kitecashtask.dagger.ApplicationContext;
import me.gurpreetsk.kitecashtask.data.local.DbHelper;
import me.gurpreetsk.kitecashtask.data.local.SharedPrefsHelper;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.data.remote.ApiHelper;

@Singleton
public class DataManager {

  private Context context;
  private ApiHelper apiHelper;
  private DbHelper dbHelper;
  private SharedPrefsHelper sharedPrefsHelper;

  private static final String TAG = DataManager.class.getSimpleName();

  @Inject
  public DataManager(@ApplicationContext Context context,
                     ApiHelper apiHelper,
                     DbHelper dbHelper,
                     SharedPrefsHelper sharedPrefsHelper) {
    this.context = context;
    this.apiHelper = apiHelper;
    this.dbHelper = dbHelper;
    this.sharedPrefsHelper = sharedPrefsHelper;
  }

  public RealmResults<User> getUsersListFromDb()
      throws Resources.NotFoundException, NullPointerException {
    return dbHelper.getAllUsers();
  }

  public void getUsersListFromServer(int page) {
    apiHelper.getUsersListFromServer(page);
  }

  public void getRepoListFromServer(String userName, int page) {
    apiHelper.getUsersRepoFromServer(userName, page);
  }

  public int getUsersListSizeFromDb() {
    return dbHelper.getUsersListSizeFromDb();
  }

}
