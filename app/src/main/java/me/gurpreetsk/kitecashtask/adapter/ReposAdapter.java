package me.gurpreetsk.kitecashtask.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.gurpreetsk.kitecashtask.R;
import me.gurpreetsk.kitecashtask.dagger.ActivityContext;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;

/**
 * Created by Gurpreet on 12/09/17.
 */

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.MyViewHolder> {

  private Context context;
  private List<UserRepo> reposList;

  private Gson gson;

  private static final String TAG = ReposAdapter.class.getSimpleName();


  public ReposAdapter(@ActivityContext Context context, List<UserRepo> reposList, Gson gson) {
    this.context = context;
    this.reposList = reposList;
    this.gson = gson;
  }

  @Override
  public ReposAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_repo_item, parent, false);
    return new ReposAdapter.MyViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    holder.textviewRepoName.setText(reposList.get(position).getName());
    holder.textviewRepoDesc.setText(reposList.get(position).getDescription());
    holder.textviewRepoStars.setText(String.valueOf(reposList.get(position).getStargazersCount()));
    holder.textviewRepoUrl.setText(reposList.get(position).getHtmlUrl());
    holder.textviewRepoWatchs.setText(String.valueOf(reposList.get(position).getWatchersCount()));
    holder.textviewCreatedOn.setText(
        String.format("Created %s", reposList.get(position).getCreatedAt().split("T")[0]));
    holder.cardView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        try {
          context.startActivity(new Intent(Intent.ACTION_VIEW,
              Uri.parse(reposList.get(holder.getAdapterPosition()).getHtmlUrl())));
        }catch (ActivityNotFoundException e){
          Log.e(TAG, "onClick: ", e);
        }
      }
    });
  }

  @Override
  public int getItemCount() {
    return reposList.size();
  }

  public void updateItems(List<UserRepo> repos) {
    reposList.addAll(repos);
    notifyDataSetChanged();
  }


  public class MyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_repo_name)
    TextView textviewRepoName;
    @BindView(R.id.textview_repo_url)
    TextView textviewRepoUrl;
    @BindView(R.id.textview_description)
    TextView textviewRepoDesc;
    @BindView(R.id.textview_stars)
    TextView textviewRepoStars;
    @BindView(R.id.textview_watch)
    TextView textviewRepoWatchs;
    @BindView(R.id.textview_created_on)
    TextView textviewCreatedOn;
    @BindView(R.id.cardview_repo)
    CardView cardView;

    public MyViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
