package me.gurpreetsk.kitecashtask.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import me.gurpreetsk.kitecashtask.R;
import me.gurpreetsk.kitecashtask.dagger.ActivityContext;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.ui.repo.RepositoriesActivity;
import me.gurpreetsk.kitecashtask.util.idlingResource.SimpleIdlingResource;

/**
 * Created by Gurpreet on 11/09/17.
 */

public class UsersAdapter extends RealmRecyclerViewAdapter<User, UsersAdapter.MyViewHolder> {

  private Context context;
  private List<User> usersList;

  // The Idling Resource which will be null in production.
  @Nullable
  private SimpleIdlingResource idlingResource;

  private static final String TAG = UsersAdapter.class.getSimpleName();


  public UsersAdapter(@ActivityContext Context context, RealmResults<User> usersList,
                      @Nullable SimpleIdlingResource idlingResource) {
    super(usersList, true);
    setHasStableIds(true);
    this.context = context;
    this.usersList = usersList;
    this.idlingResource = idlingResource;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.layout_user, parent, false);
    return new MyViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    holder.textviewLogin.setText(usersList.get(position).getLogin());
    Glide.with(context).load(usersList.get(position).getAvatarUrl()).into(holder.imageviewAvatar);
    holder.layout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (idlingResource != null) {
          idlingResource.setIdleState(false);
        }
        context.startActivity(RepositoriesActivity.getStartIntent(context,
            usersList.get(holder.getAdapterPosition()).getLogin()));
      }
    });
  }

  @Override
  public int getItemCount() {
    return usersList.size();
  }


  class MyViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textview_user_login)
    TextView textviewLogin;
    @BindView(R.id.imageview_user_avatar)
    ImageView imageviewAvatar;
    @BindView(R.id.layout_user)
    FrameLayout layout;

    MyViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

}
