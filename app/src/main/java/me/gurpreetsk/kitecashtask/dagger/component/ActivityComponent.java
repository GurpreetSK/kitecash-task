package me.gurpreetsk.kitecashtask.dagger.component;

import dagger.Component;
import me.gurpreetsk.kitecashtask.dagger.PerActivity;
import me.gurpreetsk.kitecashtask.dagger.module.ActivityModule;
import me.gurpreetsk.kitecashtask.ui.main.MainActivity;
import me.gurpreetsk.kitecashtask.ui.repo.RepositoriesActivity;

/**
 * Created by Gurpreet on 29/08/17.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

  void inject(MainActivity mainActivity);

  void inject(RepositoriesActivity repositoriesActivity);

}
