package me.gurpreetsk.kitecashtask.dagger.module;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.gurpreetsk.kitecashtask.dagger.ApplicationContext;
import me.gurpreetsk.kitecashtask.data.remote.ApiInterface;
import me.gurpreetsk.kitecashtask.util.NetworkConnection;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gurpreet on 29/08/17.
 */

@Module
public class NetModule {

  private static final String NAME_BASE_URL = "NAME_BASE_URL";

  private final String BASE_URL = "https://api.github.com";

  @Provides
  @Singleton
  Converter.Factory provideGsonConverter() {
    return GsonConverterFactory.create();
  }

  @Provides
  @Singleton
  RxJava2CallAdapterFactory provideRxCallAdapterFactory() {
    return RxJava2CallAdapterFactory.create();
  }

  @Provides
  @Singleton
  Retrofit provideRetrofit(RxJava2CallAdapterFactory factory,
                           Converter.Factory converter,
                           OkHttpClient client) {
    return new Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(factory)
        .addConverterFactory(converter)
        .client(client)
        .build();
  }

  @Provides
  @Singleton
  Cache provideHttpCache(Application application) {
    int cacheSize = 10 * 1024 * 1024;
    Cache cache = new Cache(application.getCacheDir(), cacheSize);
    return cache;
  }

  @Provides
  @Singleton
  OkHttpClient provideOkhttpClient(Cache cache, @ApplicationContext final Context context) {
    return new OkHttpClient.Builder()
        .cache(cache)
        .addInterceptor(new Interceptor() {
          @Override
          public Response intercept(@NonNull Chain chain) throws IOException {
            Request.Builder builder = chain.request().newBuilder();
            if (!NetworkConnection.isNetworkConnected(context))
              builder.cacheControl(CacheControl.FORCE_CACHE);

            return chain.proceed(builder.build());
          }
        })
        .build();
  }


  @Provides
  @Singleton
  ApiInterface provideApi(Retrofit retrofit) {
    return retrofit.create(ApiInterface.class);
  }

}
