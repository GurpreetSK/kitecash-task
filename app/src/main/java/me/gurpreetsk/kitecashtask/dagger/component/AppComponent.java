package me.gurpreetsk.kitecashtask.dagger.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import me.gurpreetsk.kitecashtask.InitApplication;
import me.gurpreetsk.kitecashtask.dagger.ApplicationContext;
import me.gurpreetsk.kitecashtask.dagger.module.AppModule;
import me.gurpreetsk.kitecashtask.dagger.module.NetModule;
import me.gurpreetsk.kitecashtask.data.DataManager;
import me.gurpreetsk.kitecashtask.data.local.DbHelper;
import me.gurpreetsk.kitecashtask.data.local.SharedPrefsHelper;

/**
 * Created by Gurpreet on 29/08/17.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

  void inject(InitApplication application);

  @ApplicationContext
  Context getContext();

  Application getApplication();

  DataManager getDataManager();

  SharedPrefsHelper getPreferenceHelper();

  DbHelper getDbHelper();

}
