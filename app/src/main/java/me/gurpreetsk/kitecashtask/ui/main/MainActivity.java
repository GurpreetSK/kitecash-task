package me.gurpreetsk.kitecashtask.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.IdlingResource;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmResults;
import me.gurpreetsk.kitecashtask.R;
import me.gurpreetsk.kitecashtask.adapter.UsersAdapter;
import me.gurpreetsk.kitecashtask.data.events.DataFetchedEvent;
import me.gurpreetsk.kitecashtask.data.events.ErrorEvent;
import me.gurpreetsk.kitecashtask.data.local.SharedPrefsHelper;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.ui.base.BaseActivity;
import me.gurpreetsk.kitecashtask.util.InfiniteScrollListener;
import me.gurpreetsk.kitecashtask.util.NetworkConnection;
import me.gurpreetsk.kitecashtask.util.idlingResource.SimpleIdlingResource;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;

public class MainActivity extends BaseActivity implements MainView {

  @BindView(R.id.recycler_view_users)
  RecyclerView recyclerviewUsers;
  @BindView(R.id.fab_goto_top)
  FloatingActionButton fab;

  @Inject
  MainPresenterImpl<MainView> presenter;
  @Inject
  SharedPrefsHelper prefsHelper;

  // The Idling Resource which will be null in production.
  @Nullable
  public static SimpleIdlingResource idlingResource;

  int pageNumber = 0;

  private static final String TAG = MainActivity.class.getSimpleName();


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    activityComponent().inject(this);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    setupRecyclerView();
    presenter.attachView(this);
    setTitle("Github Users");

    presenter.getDataFromDb();
    //if no internet connection, show toast, get from DB
    //else get from server
    if (NetworkConnection.isNetworkConnected(MainActivity.this)) {
      //TODO: cache refreshal case
      //only first page is refreshed here
      presenter.getDataFromServer(pageNumber);
      pageNumber++;
    } else {
      final Snackbar snackbar =
          showErrorSnackbar(findViewById(android.R.id.content), "No Internet Connection ");
      snackbar.setAction("OK", new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          snackbar.dismiss();
        }
      }).show();
    }
  }

  private void setupRecyclerView() {
    LinearLayoutManager gridLayoutManager;
    if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE)
      gridLayoutManager = new GridLayoutManager(MainActivity.this, 3);
    else
      gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
    recyclerviewUsers.setLayoutManager(gridLayoutManager);
    recyclerviewUsers.addItemDecoration(
        new DividerItemDecoration(MainActivity.this, DividerItemDecoration.VERTICAL));

    recyclerviewUsers.addOnScrollListener(new InfiniteScrollListener(gridLayoutManager, fab) {
      @Override
      public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        if (NetworkConnection.isNetworkConnected(MainActivity.this)) {
          prefsHelper.put(SharedPrefsHelper.USER_PAGE_NUM_KEY_ACCESS_TOKEN,
              prefsHelper.get(SharedPrefsHelper.USER_PAGE_NUM_KEY_ACCESS_TOKEN, 0) + 1);
          presenter.getDataFromServer(
              prefsHelper.get(SharedPrefsHelper.USER_PAGE_NUM_KEY_ACCESS_TOKEN, 0));
          Log.d(TAG, "onLoadMore: " +
              prefsHelper.get(SharedPrefsHelper.USER_PAGE_NUM_KEY_ACCESS_TOKEN, 0));
          pageNumber = page;
        } else {
          final Snackbar snackbar = showErrorSnackbar(findViewById(android.R.id.content),
              "No Internet Connection ");
          snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              snackbar.dismiss();
            }
          }).show();
        }
      }
    });
  }

  @OnClick(R.id.fab_goto_top)
  void gotoTop() {
    recyclerviewUsers.smoothScrollToPosition(0);
  }

  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void showError(ErrorEvent event) {
    showErrorMessage(event.getError());
    hideLoading();
  }

  @Subscribe()
  public void hideLoadingDialog(DataFetchedEvent event) {
    if (event.getType().equals("user"))
      hideLoading();
  }

  @Override
  protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override
  public void showDataToUser(RealmResults<User> userList) {
    UsersAdapter usersAdapter = new UsersAdapter(MainActivity.this, userList, idlingResource);
    recyclerviewUsers.setAdapter(usersAdapter);
  }


  /**
   * Only called from test, creates and returns a new {@link SimpleIdlingResource}.
   */
  @VisibleForTesting
  @NonNull
  public IdlingResource getIdlingResource() {
    if (idlingResource == null) {
      idlingResource = new SimpleIdlingResource();
    }
    return idlingResource;
  }

}
