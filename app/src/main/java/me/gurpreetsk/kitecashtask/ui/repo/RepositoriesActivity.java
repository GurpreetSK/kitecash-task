package me.gurpreetsk.kitecashtask.ui.repo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.gurpreetsk.kitecashtask.R;
import me.gurpreetsk.kitecashtask.adapter.ReposAdapter;
import me.gurpreetsk.kitecashtask.data.events.ErrorEvent;
import me.gurpreetsk.kitecashtask.data.events.RepoLoadEvent;
import me.gurpreetsk.kitecashtask.data.local.SharedPrefsHelper;
import me.gurpreetsk.kitecashtask.data.model.UserRepo;
import me.gurpreetsk.kitecashtask.ui.base.BaseActivity;
import me.gurpreetsk.kitecashtask.ui.main.MainActivity;
import me.gurpreetsk.kitecashtask.util.InfiniteScrollListener;
import me.gurpreetsk.kitecashtask.util.NetworkConnection;

public class RepositoriesActivity extends BaseActivity implements RepoView {

  @Inject
  RepoPresenterImpl<RepoView> presenter;
  @Inject
  SharedPrefsHelper prefsHelper;
  @Inject
  Gson gson;

  @BindView(R.id.recycler_view_repos)
  RecyclerView recyclerviewRepos;
  @BindView(R.id.fab_goto_repo_top)
  FloatingActionButton fab;

  String username;
  int currentPage = 0;

  private static final String TAG = RepositoriesActivity.class.getSimpleName();

  public static Intent getStartIntent(Context context, String userName) {
    Intent intent = new Intent(context, RepositoriesActivity.class);
    intent.putExtra("UserName", userName);
    return intent;
  }


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    activityComponent().inject(this);
    setContentView(R.layout.activity_repositories);
    ButterKnife.bind(this);
    presenter.attachView(this);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    username = getIntent().getStringExtra("UserName");
    setTitle(username + "'s Repos");

    LinearLayoutManager linearLayoutManager =
        new LinearLayoutManager(RepositoriesActivity.this, LinearLayoutManager.VERTICAL, false);
    recyclerviewRepos.setLayoutManager(linearLayoutManager);
    recyclerviewRepos.addItemDecoration(
        new DividerItemDecoration(RepositoriesActivity.this, DividerItemDecoration.VERTICAL));

    if (NetworkConnection.isNetworkConnected(RepositoriesActivity.this))
      presenter.getUserRepoDetailsFromServer(username, 0);
    else {
      Snackbar snackbar =
          showErrorSnackbar(findViewById(android.R.id.content), "No Internet Connection ");
      snackbar.setAction("RETRY", new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          presenter.getUserRepoDetailsFromServer(username, currentPage);
        }
      }).show();
    }

    recyclerviewRepos.addOnScrollListener(new InfiniteScrollListener(linearLayoutManager, fab) {
      @Override
      public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
        if (NetworkConnection.isNetworkConnected(RepositoriesActivity.this)) {
          presenter.getUserRepoDetailsFromServer(username, page + 1);
          currentPage = page + 1;
        } else
          showErrorSnackbar(findViewById(android.R.id.content), "No Internet Connection ")
              .setAction("RETRY", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  presenter.getUserRepoDetailsFromServer(username, currentPage);
                }
              }).show();
      }
    });
  }

  @Override
  protected void onStart() {
    super.onStart();
    EventBus.getDefault().register(this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    EventBus.getDefault().unregister(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @OnClick(R.id.fab_goto_repo_top)
  void gotoTop() {
    recyclerviewRepos.smoothScrollToPosition(0);
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onRepoLoaded(RepoLoadEvent event) {
    setupUserReposRecyclerView(event.getRepoList());
    hideLoading();
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void showError(ErrorEvent event) {
    if (event.getError().toLowerCase().contains("please retry")) {
      final Snackbar snackbar = showErrorSnackbar(findViewById(android.R.id.content), event.getError());
      snackbar.setAction("RETRY", new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          presenter.getUserRepoDetailsFromServer(username, currentPage);
        }
      }).show();
    } else
      showErrorMessage(event.getError());
    hideLoading();
  }

  @Override
  public void setupUserReposRecyclerView(List<UserRepo> reposList) {
    if (recyclerviewRepos.getAdapter() == null) {
      recyclerviewRepos.setAdapter(new ReposAdapter(RepositoriesActivity.this, reposList, gson));
      if (MainActivity.idlingResource != null) {
        MainActivity.idlingResource.setIdleState(true);
      }
    } else {
      ((ReposAdapter) recyclerviewRepos.getAdapter()).updateItems(reposList);
    }
  }


}
