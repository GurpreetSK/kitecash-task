package me.gurpreetsk.kitecashtask.ui.main;

import javax.inject.Inject;

import me.gurpreetsk.kitecashtask.data.DataManager;
import me.gurpreetsk.kitecashtask.ui.base.BasePresenterImpl;

/**
 * Created by Gurpreet on 29/08/17.
 */

public class MainPresenterImpl<V extends MainView> extends BasePresenterImpl<V>
    implements MainPresenter<V> {

  private DataManager dataManager;

  @Inject
  public MainPresenterImpl(DataManager dataManager) {
    this.dataManager = dataManager;
  }

  @Override
  public void getDataFromServer(int page) {
    if (getDbDataSize() <= page * 30) {
      if (page == 0)
        getView().showLoading();
      dataManager.getUsersListFromServer(page);
    }
  }

  @Override
  public void getDataFromDb() {
    getView().showDataToUser(dataManager.getUsersListFromDb());
  }

  @Override
  public int getDbDataSize() {
    return dataManager.getUsersListSizeFromDb();
  }

}
