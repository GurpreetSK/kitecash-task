package me.gurpreetsk.kitecashtask.ui.main;

import io.realm.RealmResults;
import me.gurpreetsk.kitecashtask.data.model.User;
import me.gurpreetsk.kitecashtask.ui.base.BaseMvpView;

/**
 * Created by Gurpreet on 29/08/17.
 */

public interface MainView extends BaseMvpView {

  void showDataToUser(RealmResults<User> userList);

}
