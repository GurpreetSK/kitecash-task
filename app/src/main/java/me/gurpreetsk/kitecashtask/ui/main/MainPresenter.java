package me.gurpreetsk.kitecashtask.ui.main;

import me.gurpreetsk.kitecashtask.ui.base.BasePresenter;

/**
 * Created by Gurpreet on 29/08/17.
 */

public interface MainPresenter<V extends MainView> extends BasePresenter<V> {

  void getDataFromServer(int page);

  void getDataFromDb();

  int getDbDataSize();

}
