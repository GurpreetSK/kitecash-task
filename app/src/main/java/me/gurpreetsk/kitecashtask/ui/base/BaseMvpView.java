package me.gurpreetsk.kitecashtask.ui.base;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by Gurpreet on 29/08/17.
 */

public interface BaseMvpView {

  void showLoading();

  void hideLoading();

  void showErrorMessage(String errorMessage);

  Snackbar showErrorSnackbar(View view, String errorMessage);

  void showSuccessMessage(String successMessage);

}
