package me.gurpreetsk.kitecashtask.ui.repo;

import javax.inject.Inject;

import me.gurpreetsk.kitecashtask.data.DataManager;
import me.gurpreetsk.kitecashtask.ui.base.BasePresenterImpl;

/**
 * Created by Gurpreet on 12/09/17.
 */

public class RepoPresenterImpl<V extends RepoView>
    extends BasePresenterImpl<V> implements RepoPresenter<V> {

  private DataManager dataManager;

  @Inject
  public RepoPresenterImpl(DataManager dataManager) {
    this.dataManager = dataManager;
  }

  @Override
  public void getUserRepoDetailsFromServer(String userName, int page) {
    if (page == 0 || page == 1)
      getView().showLoading();
    dataManager.getRepoListFromServer(userName, page);
  }

}
