package me.gurpreetsk.kitecashtask.ui.repo;

import me.gurpreetsk.kitecashtask.ui.base.BasePresenter;

/**
 * Created by Gurpreet on 12/09/17.
 */

public interface RepoPresenter<V extends RepoView> extends BasePresenter<V> {

  void getUserRepoDetailsFromServer(String userName, int page);

}
