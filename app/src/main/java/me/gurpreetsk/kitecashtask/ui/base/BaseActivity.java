package me.gurpreetsk.kitecashtask.ui.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import me.gurpreetsk.kitecashtask.InitApplication;
import me.gurpreetsk.kitecashtask.dagger.component.ActivityComponent;
import me.gurpreetsk.kitecashtask.dagger.component.DaggerActivityComponent;
import me.gurpreetsk.kitecashtask.dagger.module.ActivityModule;
import me.gurpreetsk.kitecashtask.ui.main.MainActivity;
import me.gurpreetsk.kitecashtask.util.DialogUtils;

/**
 * Created by Gurpreet on 29/08/17.
 */

public class BaseActivity extends AppCompatActivity implements BaseMvpView {

  private ActivityComponent mActivityComponent;
  private ProgressDialog progressDialog;

  public ActivityComponent activityComponent() {
    if (mActivityComponent == null)
      mActivityComponent = DaggerActivityComponent.builder()
          .activityModule(new ActivityModule(this))
          .appComponent(InitApplication.getApplication(this).getAppComponent())
          .build();
    return mActivityComponent;
  }

  @TargetApi(Build.VERSION_CODES.M)
  public void requestPermissionsSafely(String[] permissions, int requestCode) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      requestPermissions(permissions, requestCode);
    }
  }

  @TargetApi(Build.VERSION_CODES.M)
  public boolean hasPermission(String permission) {
    return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
        checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public void showLoading() {
    hideLoading();
    progressDialog = DialogUtils.showLoadingDialog(this);
  }

  @Override
  public void hideLoading() {
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.cancel();
    }
  }

  public void hideLoadingAndSetIdlingResourceTrue() {
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.cancel();
    }
    if (MainActivity.idlingResource != null) {
      MainActivity.idlingResource.setIdleState(true);
    }
  }

  @Override
  public void showErrorMessage(String errorMessage) {
    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
  }

  @Override
  public Snackbar showErrorSnackbar(View view, String errorMessage) {
    return Snackbar.make(view, errorMessage, Snackbar.LENGTH_INDEFINITE);
  }

  @Override
  public void showSuccessMessage(String successMessage) {
    Toast.makeText(this, successMessage, Toast.LENGTH_SHORT).show();
  }

}
