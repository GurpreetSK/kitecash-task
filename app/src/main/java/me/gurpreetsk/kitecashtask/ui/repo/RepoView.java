package me.gurpreetsk.kitecashtask.ui.repo;

import java.util.List;

import me.gurpreetsk.kitecashtask.data.model.UserRepo;
import me.gurpreetsk.kitecashtask.ui.base.BaseMvpView;

/**
 * Created by Gurpreet on 12/09/17.
 */

public interface RepoView extends BaseMvpView {

  void setupUserReposRecyclerView(List<UserRepo> userList);

}
