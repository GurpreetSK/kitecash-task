package me.gurpreetsk.kitecashtask;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import io.realm.Realm;
import me.gurpreetsk.kitecashtask.dagger.component.AppComponent;
import me.gurpreetsk.kitecashtask.dagger.component.DaggerAppComponent;
import me.gurpreetsk.kitecashtask.dagger.module.AppModule;
import me.gurpreetsk.kitecashtask.dagger.module.NetModule;
import me.gurpreetsk.kitecashtask.data.DataManager;

/**
 * Created by Gurpreet on 29/08/17.
 */

public class InitApplication extends Application {

  AppComponent appComponent;

  @Inject
  DataManager datamanager;

  @Override
  public void onCreate() {
    super.onCreate();
    Realm.init(this);
    appComponent = DaggerAppComponent.builder()
        .appModule(new AppModule(this))
        .netModule(new NetModule())
        .build();
    appComponent.inject(this);
  }

  public static InitApplication getApplication(Context context) {
    return (InitApplication) context.getApplicationContext();
  }

  public AppComponent getAppComponent() {
    return appComponent;
  }

  // Needed to replace the component with a test specific one
  public void setComponent(AppComponent applicationComponent) {
    appComponent = applicationComponent;
  }

}
